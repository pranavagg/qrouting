package routing;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import routing.util.RoutingInfo;
import util.Tuple;
import core.Connection;
import core.DTNHost;
import core.Message;
import core.Settings;
import core.SimClock;

public class BaseCQRouter extends ActiveRouter {

	/** Q router's setting namespace ({@value})*/ 
	public static final String Q_NS = "BaseQRouter";
	
	public static final String SECONDS_IN_UNIT_S ="secondsInTimeUnit";
	
	/* Learning Rate */
	public static final double DEFAULT_ETA = 0.5;
	
	private double eta;
	
	public static final String ETA_S = "eta";
	
	/** Q Table */
	private Map<DTNHost, Map<DTNHost, Double>> q_table;
	/** last connected time of every node */
	private Map<DTNHost, Double> con_table;
	
	private Map<DTNHost, Double> last_con_table;

	private int counter;
	
	/** the value of nrof seconds in time unit -setting */
	public int secondsInTimeUnit;

	public Map<DTNHost, Map<DTNHost, Double>> c_table;

	public Map<DTNHost, Map<DTNHost, Double>> u_table;

	public int c_time_unit = 60;

	public double lambda = 0.98;

	public BaseCQRouter(Settings s) {
		super(s);
		Settings QSettings = new Settings(Q_NS);
		secondsInTimeUnit = QSettings.getInt(SECONDS_IN_UNIT_S);
		if (QSettings.contains(ETA_S)) {
			eta = QSettings.getDouble(ETA_S);
		}
		else {
			eta = DEFAULT_ETA;
		}
		initTables();
	}

	protected BaseCQRouter(BaseCQRouter r) {
		super(r);
		this.secondsInTimeUnit = r.secondsInTimeUnit;
		this.eta = r.eta;
		initTables();
	}

	@Override
	public MessageRouter replicate() {
		BaseCQRouter r = new BaseCQRouter(this);
		return r;
	}

	private void initTables() {
		this.q_table = new HashMap<DTNHost, Map<DTNHost, Double>>();
		this.c_table = new HashMap<DTNHost, Map<DTNHost, Double>>();
		this.u_table = new HashMap<DTNHost, Map<DTNHost, Double>>();
		this.con_table = new HashMap<DTNHost, Double>();
		this.last_con_table = new HashMap<DTNHost, Double>();
	}

	@Override
	public void changedConnection(Connection con) {
		super.changedConnection(con);
		
		if (con.isUp()) {
			DTNHost otherHost = con.getOtherNode(getHost());
			updateLastConnectionTable(otherHost);
			updateQTable(otherHost);
			updateConnectionTable(otherHost);
		}
	}

	private void updateLastConnectionTable(DTNHost otherHost){
		double timeDiff = (SimClock.getTime() / secondsInTimeUnit) - con_table.getOrDefault(otherHost, 0.0);
		last_con_table.put(otherHost, timeDiff);
//		if (!(SimClock.getTime()/secondsInTimeUnit == timeDiff))
//		System.out.println(getHost() + " --> " + otherHost + " curr=" + SimClock.getTime()/secondsInTimeUnit + " new=" + timeDiff);
	}

	private void updateQTable(DTNHost otherHost){
		DTNHost self = getHost();
		if (q_table.containsKey(otherHost)){
			double timeDiff = last_con_table.getOrDefault(otherHost, 0.0);
			double old_value = q_table.get(otherHost).getOrDefault(self, 0.0);

			double c_value = get_c_value(otherHost, self);

			double e = Math.max(c_value, 1 - c_value);

			double new_value = updateQValue(timeDiff, 0.0, old_value, e);
//			System.out.println(self + " --> " + otherHost + " q=" + timeDiff + " old=" + old_value + " new=" + new_value);
			q_table.get(otherHost).put(self, new_value);

			double new_c_value = update_c_value(c_value, c_value, e);
			c_table.get(otherHost).put(self, new_c_value);
			u_table.get(otherHost).put(self, SimClock.getTime());
		}
		else{
			Map<DTNHost, Double> innerMap = new HashMap<DTNHost, Double>();
			Map<DTNHost, Double> c_map = new HashMap<DTNHost, Double>();
			Map<DTNHost, Double> u_map = new HashMap<DTNHost, Double>();
			double time = last_con_table.get(otherHost);
			
			innerMap.put(self, time);
			c_map.put(self, 0.0);
			u_map.put(self, SimClock.getTime());

			q_table.put(otherHost, innerMap);
			c_table.put(otherHost, c_map);
			u_table.put(otherHost, u_map);
		}
	}

	public double update_c_value(double old_value, double est_value, double e){
		return old_value + (e * (est_value - old_value));
	}

	public double get_c_value(DTNHost destination, DTNHost y){
		if (u_table.containsKey(destination) && c_table.containsKey(destination)){
			double timeDiff = SimClock.getTime() - u_table.get(destination).getOrDefault(y, 0.0);
			int l = (int) timeDiff / c_time_unit;
			return Math.pow(lambda, l) * c_table.get(destination).getOrDefault(y, 0.0);
		}
		else
			return 0.0;
	}

	public double updateQValue(double q, double t, double old_value, double e){
		return old_value + (e * ((q + t) - old_value));
	}

	private void updateConnectionTable(DTNHost otherHost){
		double time = (SimClock.getTime()) / secondsInTimeUnit;
		con_table.put(otherHost, time);
	}

	@Override
	public void update() {
		super.update();
		if (isTransferring() || !canStartTransfer()) {
			return; // transferring, don't try other connections yet
		}
		
		// Try first the messages that can be delivered to final recipient
		if (exchangeDeliverableMessages() != null) {
			return; // started a transfer, don't try others (yet)
		}
		tryOtherMessages();
	}

	public Tuple<Message, Connection> tryOtherMessages() {
		List<Tuple<Message, Connection>> messages = 
			new ArrayList<Tuple<Message, Connection>>(); 
	
		Collection<Message> msgCollection = getMessageCollection();

		for (Message m : msgCollection) {
			DTNHost destination = m.getTo();
			if (q_table.containsKey(destination)){

				double min = Double.MAX_VALUE;
				Connection min_connection = null;

				for (Connection con : getConnections()) {
					DTNHost other = con.getOtherNode(getHost());
					double q_value = q_table.get(destination).getOrDefault(other, Double.MAX_VALUE);
					if(q_value <= min){
						min = q_value;
						min_connection = con;
					}
				}

				double self_q_value = q_table.get(destination).getOrDefault(getHost(), Double.MAX_VALUE);
				if (self_q_value < min){
					min = self_q_value;
					min_connection = null;
				}

				if (min_connection != null){
					messages.add(new Tuple<Message, Connection>(m,min_connection));
				}
			}
			else{
				List<Connection> cons = getConnections();
				if (!cons.isEmpty()){
					Connection first = cons.get(0);
					messages.add(new Tuple<Message, Connection>(m, first));
				}
			}
		}

		if (messages.size() == 0) {
			return null;
		}

		this.sortByQueueMode(messages);
		return tryMessagesForConnected(messages);
	}

	public void updateQTableForwarding(DTNHost destination, DTNHost other, BaseCQRouter othRouter, Message m){
		// System.out.println("From: " + getHost() + "   To: " + other + "    Desitnation: " + destination);

		Message init_message = getMessage(m.getId());

		// System.out.println("Message Recieved At: " + init_message.getReceiveTime());

		// double q = last_con_table.getOrDefault(other, 0.0);
		double q = (SimClock.getTime() - init_message.getReceiveTime()) / secondsInTimeUnit;

		Tuple<Double, Double> min_y = othRouter.get_min_for_destination(destination);
		double t = min_y.getValue();
		double c_est = min_y.getKey();

		double c_old = get_c_value(destination, other);

		double e = Math.max(c_est, 1 - c_old);

		if (q_table.containsKey(destination)){
			double old_value = q_table.get(destination).getOrDefault(other, 0.0);
			double new_value = updateQValue(q, t, old_value, e);
			q_table.get(destination).put(other, new_value);

			double new_c_value = update_c_value(c_old, c_est, e);

			c_table.get(destination).put(other, new_c_value);
			u_table.get(destination).put(other, SimClock.getTime());
		}
		else{
			Map<DTNHost, Double> inner = new HashMap<DTNHost, Double>();
			Map<DTNHost, Double> c_map = new HashMap<DTNHost, Double>();
			Map<DTNHost, Double> u_map = new HashMap<DTNHost, Double>();

			double new_value = q + t;
			
			inner.put(other, new_value);
			double new_c_value = update_c_value(c_old, c_est, e);
			c_map.put(other, new_c_value);
			u_map.put(other, SimClock.getTime());

			q_table.put(destination, inner);
			c_table.put(destination, c_map);
			u_table.put(destination, u_map);
//			System.out.println(getHost() + " --> " + other + " DEstinatio:" + destination + " q=" + q + " old=" + "NULL" + " new=" + new_value);
		}
	}

	public Tuple<Double, Double> get_min_for_destination(DTNHost destination){
		double min = Double.MAX_VALUE;
		DTNHost min_host = null;
		double c_est;

		if (q_table.containsKey(destination)){
			Map<DTNHost, Double> inner = q_table.get(destination);
			for (Map.Entry<DTNHost, Double> f : inner.entrySet()) {
				Double innerValue = f.getValue();
				if (innerValue < min){
					min = innerValue;
					min_host = f.getKey();
				}
			}
		}
		if (min == Double.MAX_VALUE)
			min = 0;
		if (min_host!=null){
			c_est = get_c_value(destination, min_host);
		}
		else
			c_est = 0;
		return new Tuple<Double, Double>(c_est, min);
	}

	@Override
	public RoutingInfo getRoutingInfo() {
		RoutingInfo top = super.getRoutingInfo();
		RoutingInfo ri = new RoutingInfo(String.format("Update Counter = %d", counter));
		top.addMoreInfo(ri);
		return top;
	}

	// @Override
	// protected void transferDone(Connection con) {
	// 	DTNHost other = con.getOtherNode(getHost());
	// 	BaseQRouter othRouter = (BaseQRouter)other.getRouter();
	// 	updateQTableForwarding(destination, other, othRouter);
	// 	System.out.println("Done");
	// }


	@Override
	public Message messageTransferred(String id, DTNHost from) {
		Message m = super.messageTransferred(id, from);
		BaseCQRouter fromRouter = (BaseCQRouter)from.getRouter();
		DTNHost destination = m.getTo();
		fromRouter.updateQTableForwarding(destination, getHost(), this, m);
		// System.out.println("Done");
		return m;
	}

	public Map<DTNHost, Map<DTNHost, Double>> getQTable(){
		return q_table;
	}

	public Map<DTNHost, Double> getConnectionTable(){
		return con_table;
	}
	
	public Map<DTNHost, Double> getLastConnectionTable(){
		return last_con_table;
	}

	public void putNewQValueForKnownDestination(DTNHost destination, DTNHost other, double new_value){
		q_table.get(destination).put(other, new_value);
	}

	public void putNewQValueForUnknownDestination(DTNHost destination, DTNHost other, double new_value){
		Map<DTNHost, Double> inner = new HashMap<DTNHost, Double>();
		inner.put(other, new_value);
		q_table.put(destination, inner);
	}

}