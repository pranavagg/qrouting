package routing;

import java.util.ArrayList;
import java.util.List;

import core.Connection;
import core.Message;
import core.Settings;

public class NEpidemicRouter extends ActiveRouter {
	
	public static final String NEPIDEMIC_NS = "NEpidemicRouter";
	public static final String N_S ="N";
	public static final int DEFAULT_N = 1;
	
	private int n;

	@Override
	public MessageRouter replicate() {
		// TODO Auto-generated method stub
		return new NEpidemicRouter(this);
	}
	
	public NEpidemicRouter(Settings s) {
		super(s);
		Settings NEpideimicSettings = new Settings(NEPIDEMIC_NS);
		if (NEpideimicSettings.contains(N_S)) {
			n = NEpideimicSettings.getInt(N_S);
		}
		else {
			n = DEFAULT_N;
		}
	}
	
	/**
	 * Copy constructor.
	 * @param r The router prototype where setting values are copied from
	 */
	protected NEpidemicRouter(NEpidemicRouter r) {
		super(r);
		this.n = r.n;
		//TODO: copy epidemic settings here (if any)
	}
	
	@Override
	public void update() {
		super.update();
		if (isTransferring() || !canStartTransfer()) {
			return; // transferring, don't try other connections yet
		}
		
		// Try first the messages that can be delivered to final recipient
		if (exchangeDeliverableMessages() != null) {
			return; // started a transfer, don't try others (yet)
		}
		
		// then try any/all message to any/all connection
		tryAllMessagesToNConnections();
	}
	
	protected Connection tryAllMessagesToNConnections(){
		List<Connection> connections = getConnections();
		if (connections.size() == 0 || this.getNrofMessages() == 0 || connections.size() < n) {
			return null;
		}

		List<Message> messages = 
			new ArrayList<Message>(this.getMessageCollection());
		this.sortByQueueMode(messages);

		return tryMessagesToConnections(messages, connections);
	}
}
