package routing;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import routing.util.RoutingInfo;
import util.Tuple;
import core.Connection;
import core.DTNHost;
import core.Message;
import core.Settings;
import core.SimClock;

import org.apache.commons.math3.distribution.PoissonDistribution;

public class QRouter extends BaseQRouter {

	public QRouter(Settings s) {
		super(s);
	}

	protected QRouter(QRouter r) {
		super(r);
	}

	@Override
	public MessageRouter replicate() {
		QRouter r = new QRouter(this);
		return r;
	}


	@Override
	public Tuple<Message, Connection> tryOtherMessages() {
		List<Tuple<Message, Connection>> messages = 
			new ArrayList<Tuple<Message, Connection>>(); 
	
		Collection<Message> msgCollection = getMessageCollection();
		
		/* for all connected hosts collect all messages that have a higher
		   probability of delivery by the other host */
		
		for (Message m : msgCollection) {
			Tuple<Message, Connection> max_tuple=null;
			double max = 0;
			DTNHost destination = m.getTo();
			for (Connection con : getConnections()) {
				DTNHost other = con.getOtherNode(getHost());
				QRouter othRouter = (QRouter)other.getRouter();
				
				double self, otherProb;
				
				if (othRouter.isTransferring()) {
					continue; // skip hosts that are transferring
				}
				if (othRouter.hasMessage(m.getId())) {
					continue; // skip messages that the other one has
				}
				self = get_probability(destination, other);
				otherProb = othRouter.get_probability(destination, getHost());
				if (self < otherProb){
					if (otherProb > max){
						max = otherProb;
						max_tuple = new Tuple<Message, Connection>(m,con);
					}
				}
			}
			if (max_tuple != null){
				messages.add(max_tuple);
			}
		}
		
		if (messages.size() == 0) {
			return null;
		}
		
//		Collections.sort(messages, new TupleComparator());
		
		this.sortByQueueMode(messages);
		return tryMessagesForConnected(messages);	// try to send messages
	}
	
	/**
	 * Comparator for Message-Connection-Tuples that orders the tuples by
	 * their delivery probability by the host on the other side of the 
	 * connection (GRTRMax)
	 */
	private class TupleComparator implements Comparator 
		<Tuple<Message, Connection>> {

		public int compare(Tuple<Message, Connection> tuple1,
				Tuple<Message, Connection> tuple2) {
			// delivery probability of tuple1's message with tuple1's connection
			double p1 = ((QRouter)tuple1.getValue().
					getOtherNode(getHost()).getRouter()).get_probability(
					tuple1.getKey().getTo(), getHost());
			// -"- tuple2...
			double p2 = ((QRouter)tuple2.getValue().
					getOtherNode(getHost()).getRouter()).get_probability(
					tuple2.getKey().getTo(), getHost());

			// bigger probability should come first
			if (p2-p1 == 0) {
				/* equal probabilities -> let queue mode decide */
				return compareByQueueMode(tuple1.getKey(), tuple2.getKey());
			}
			else if (p2-p1 < 0) {
				return -1;
			}
			else {
				return 1;
			}
		}
	}
	
	@Override
	public RoutingInfo getRoutingInfo() {
		Map<DTNHost, Map<DTNHost, Double>> q_table = getQTable();
		Map<DTNHost, Double> con_table = getConnectionTable();
		Map<DTNHost, Double> last_con_table = getLastConnectionTable();
		RoutingInfo top = super.getRoutingInfo();
		RoutingInfo ri = new RoutingInfo(con_table.size() + 
				" Connection Table Data");
		
		RoutingInfo ri2 = new RoutingInfo(q_table.size() + 
				" Q Table Data");
		
		RoutingInfo ri3 = new RoutingInfo(last_con_table.size() + " last connection period");
		
		for (Map.Entry<DTNHost, Double> e : con_table.entrySet()) {
			DTNHost host = e.getKey();
			Double value = e.getValue();
			
			ri.addMoreInfo(new RoutingInfo(String.format("%s : %.6f", 
					host, value)));
		}
		
		for (Map.Entry<DTNHost, Double> e : last_con_table.entrySet()) {
			DTNHost host = e.getKey();
			Double value = e.getValue();
			
			ri3.addMoreInfo(new RoutingInfo(String.format("%s : %.6f", 
					host, value)));
		}
		
		for (Map.Entry<DTNHost, Map<DTNHost, Double>> e : q_table.entrySet()) {
			DTNHost host = e.getKey();
			RoutingInfo inner = new RoutingInfo("Destination " + host);
			Map<DTNHost, Double> value = e.getValue();
			for (Map.Entry<DTNHost, Double> f : value.entrySet()) {
				DTNHost innerHost = f.getKey();
				Double innerValue = f.getValue();
				inner.addMoreInfo(new RoutingInfo(String.format("%s : %.6f", 
						innerHost, innerValue)));
			}
			ri2.addMoreInfo(inner);
		}
		
		top.addMoreInfo(ri);
		top.addMoreInfo(ri2);
		top.addMoreInfo(ri3);
		return top;
	}
	
	private double getPoissonProbability(double mean, int x){
		PoissonDistribution p = new PoissonDistribution(mean);
		return p.probability(x);
	}
	
	private double get_probability_exclusive(DTNHost dest, DTNHost y){
		double max=0;
		Map<DTNHost, Map<DTNHost, Double>> q_table = getQTable();
		Map<DTNHost, Double> con_table = getConnectionTable();
		for (Map.Entry<DTNHost, Double> f : q_table.get(dest).entrySet()) {
			DTNHost a = f.getKey();
			Double innerValue = f.getValue();  // Qx(d, a)
			
			if (a.equals(y))
				continue;
			else if (a.equals(getHost()))
				continue;
			else{
				double self_prob, transitive, total, q;
				int time;
				
				if (q_table.containsKey(a)){
					if (q_table.get(a).containsKey(getHost()))
						q = q_table.get(a).get(getHost());  // Qx(a, x)
					else
						continue;
				}
				else
					continue;
				if (con_table.containsKey(a))
					time = (int)((SimClock.getTime() / secondsInTimeUnit) - con_table.get(a));  // lx(a)
				else
					continue;
				self_prob = getPoissonProbability(q, time);
				
				double q_dash = innerValue - q;
				if (q_dash > 0){
					transitive = getPoissonProbability(q_dash, (int) q_dash);
				}
				else
					continue;
				total = self_prob * transitive;
				if (total > max)
					max = total;
			}	
		}
		return max;
	}
	
	private double get_direct_probability(DTNHost dest){
		Map<DTNHost, Map<DTNHost, Double>> q_table = getQTable();
		Map<DTNHost, Double> con_table = getConnectionTable();
		double q, p;
		int time;
		if (q_table.get(dest).containsKey(getHost())){
			q = q_table.get(dest).get(getHost());  // Qx(d, x)
			if (con_table.containsKey(dest)){
				time = (int)((SimClock.getTime() / secondsInTimeUnit) - con_table.get(dest));  // lx(d)
				p = getPoissonProbability(q, time);
				return p;
			}
		}
		return 0;
	}
	
	public double get_probability(DTNHost dest, DTNHost y){
		Map<DTNHost, Map<DTNHost, Double>> q_table = getQTable();
		if (q_table.containsKey(dest)){
			double exclusive = get_probability_exclusive(dest, y);
			double direct = get_direct_probability(dest);
			if (exclusive > direct)
				return exclusive;
			else
				return direct;
		}
		else{
			return 0;
		}
	}
}
