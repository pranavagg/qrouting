package routing;

import java.util.HashMap;
import util.Tuple;

import core.DTNHost;
import core.Message;
import core.Settings;
import core.SimClock;
import java.util.Map;


public class BaseCDQRouter extends BaseCQRouter {
	public BaseCDQRouter(Settings s) {
		super(s);
	}

	protected BaseCDQRouter(BaseCDQRouter r) {
		super(r);
	}

	@Override
	public MessageRouter replicate() {
		BaseCDQRouter r = new BaseCDQRouter(this);
		return r;
	}

	@Override
	public Message messageTransferred(String id, DTNHost from) {
		Message m = super.messageTransferred(id, from);
		DTNHost source = m.getFrom();
		BaseCDQRouter fromRouter = (BaseCDQRouter)from.getRouter();

		// System.out.println("Source: " + source + "    from: " + from + "    message: " + m);

		backward_exploration(source, from, fromRouter, m);

		return m;
	}

	public void backward_exploration(DTNHost source, DTNHost from, BaseCDQRouter fromRouter, Message m){
		double q = fromRouter.get_other_router_q_value(m);
		Tuple<Double, Double> min_x = fromRouter.get_min_for_destination(source);
		double t = min_x.getValue();
		double c_est = min_x.getKey();
		double c_old = get_c_value(source, from);
		Map<DTNHost, Map<DTNHost, Double>> q_table = getQTable();
		double e = Math.max(c_est, 1 - c_old);

		if (q_table.containsKey(source)){
			double old_value = q_table.get(source).getOrDefault(from, 0.0);
			double new_value = updateQValue(q, t, old_value, e);
			putNewQValueForKnownDestination(source, from, new_value);

			double new_c_value = update_c_value(c_old, c_est, e);

			c_table.get(source).put(from, new_c_value);
			u_table.get(source).put(from, SimClock.getTime());
		}
		else{
			double new_value = q + t;
			Map<DTNHost, Double> c_map = new HashMap<DTNHost, Double>();
			Map<DTNHost, Double> u_map = new HashMap<DTNHost, Double>();
			putNewQValueForUnknownDestination(source, from, new_value);

			double new_c_value = update_c_value(c_old, c_est, e);
			c_map.put(from, new_c_value);
			u_map.put(from, SimClock.getTime());

			c_table.put(source, c_map);
			u_table.put(source, u_map);
		}

	}

	public double get_other_router_q_value(Message m){
		Message init_message = getMessage(m.getId());
		double q = (SimClock.getTime() - init_message.getReceiveTime()) / secondsInTimeUnit;
		return q;
	}
}