package routing;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import routing.util.RoutingInfo;
import util.Tuple;
import core.Connection;
import core.DTNHost;
import core.Message;
import core.Settings;
import core.SimClock;

public class BasePQRouter extends ActiveRouter {

	/** Q router's setting namespace ({@value})*/ 
	public static final String Q_NS = "BaseQRouter";
	
	public static final String SECONDS_IN_UNIT_S ="secondsInTimeUnit";
	
	/* Learning Rate */
	public static final double DEFAULT_ETA = 0.5;
	
	private double eta;
	
	public static final String ETA_S = "eta";
	
	/** Q Table */
	private Map<DTNHost, Map<DTNHost, Double>> q_table;
	/** last connected time of every node */
	private Map<DTNHost, Double> con_table;
	
	private Map<DTNHost, Double> last_con_table;

	private int counter;
	
	/** the value of nrof seconds in time unit -setting */
	public int secondsInTimeUnit;

	public Map<DTNHost, Map<DTNHost, Double>> c_table;

	public Map<DTNHost, Map<DTNHost, Double>> u_table;
	public Map<DTNHost, Map<DTNHost, Double>> b_table;
	public Map<DTNHost, Map<DTNHost, Double>> r_table;

	public double alpha = 1;
	public double beta = 0.7;
	public double gamma = 0.9;

	public BasePQRouter(Settings s) {
		super(s);
		Settings QSettings = new Settings(Q_NS);
		secondsInTimeUnit = QSettings.getInt(SECONDS_IN_UNIT_S);
		if (QSettings.contains(ETA_S)) {
			eta = QSettings.getDouble(ETA_S);
		}
		else {
			eta = DEFAULT_ETA;
		}
		initTables();
	}

	protected BasePQRouter(BasePQRouter r) {
		super(r);
		this.secondsInTimeUnit = r.secondsInTimeUnit;
		this.eta = r.eta;
		initTables();
	}

	@Override
	public MessageRouter replicate() {
		BasePQRouter r = new BasePQRouter(this);
		return r;
	}

	private void initTables() {
		this.q_table = new HashMap<DTNHost, Map<DTNHost, Double>>();
		this.c_table = new HashMap<DTNHost, Map<DTNHost, Double>>();
		this.u_table = new HashMap<DTNHost, Map<DTNHost, Double>>();
		this.b_table = new HashMap<DTNHost, Map<DTNHost, Double>>();
		this.r_table = new HashMap<DTNHost, Map<DTNHost, Double>>();
		this.con_table = new HashMap<DTNHost, Double>();
		this.last_con_table = new HashMap<DTNHost, Double>();
	}

	@Override
	public void changedConnection(Connection con) {
		super.changedConnection(con);
		
		if (con.isUp()) {
			DTNHost otherHost = con.getOtherNode(getHost());
			updateLastConnectionTable(otherHost);
			updateQTable(otherHost);
			updateConnectionTable(otherHost);
		}
	}

	private void updateLastConnectionTable(DTNHost otherHost){
		double timeDiff = (SimClock.getTime() / secondsInTimeUnit) - con_table.getOrDefault(otherHost, 0.0);
		last_con_table.put(otherHost, timeDiff);
//		if (!(SimClock.getTime()/secondsInTimeUnit == timeDiff))
//		System.out.println(getHost() + " --> " + otherHost + " curr=" + SimClock.getTime()/secondsInTimeUnit + " new=" + timeDiff);
	}

	private void updateQTable(DTNHost otherHost){
		DTNHost self = getHost();
		if (q_table.containsKey(otherHost)){
			double timeDiff = last_con_table.getOrDefault(otherHost, 0.0);
			double old_value = q_table.get(otherHost).getOrDefault(self, 0.0);
			double new_value = updateQValue(timeDiff, 0.0, old_value);
//			System.out.println(self + " --> " + otherHost + " q=" + timeDiff + " old=" + old_value + " new=" + new_value);
			q_table.get(otherHost).put(self, new_value);
			double b_value = b_table.get(otherHost).getOrDefault(self, 0.0);
			double delta_q = timeDiff - old_value;
			b_table.get(otherHost).put(self, Math.min(b_value, new_value));
			if (delta_q < 0){
				double last_edited_time = u_table.get(otherHost).get(self);
				double delta_time = (SimClock.getTime() - last_edited_time) / secondsInTimeUnit;
				double delta_r_value = delta_q / (delta_time);
				double new_r_value = r_table.get(otherHost).get(self) + (beta * delta_r_value);
				r_table.get(otherHost).put(self, new_r_value);
			}
			else if(delta_q > 0){
				double new_r_value = gamma * r_table.get(otherHost).getOrDefault(self, 0.0);
				r_table.get(otherHost).put(self, new_r_value);
			}
			u_table.get(otherHost).put(self, SimClock.getTime());
		}
		else{
			Map<DTNHost, Double> innerMap = new HashMap<DTNHost, Double>();
			Map<DTNHost, Double> new_b_map = new HashMap<DTNHost, Double>();
			Map<DTNHost, Double> new_r_map = new HashMap<DTNHost, Double>();
			Map<DTNHost, Double> new_u_map = new HashMap<DTNHost, Double>();
			double time = last_con_table.get(otherHost);
			double curr_time = SimClock.getTime() / secondsInTimeUnit;
			innerMap.put(self, time);
			// new Row Addition in table
			q_table.put(otherHost, innerMap);
			new_b_map.put(self, time);
			b_table.put(otherHost, new_b_map);

			double delta_r_value = time / curr_time;
			new_r_map.put(self, beta * delta_r_value);
			r_table.put(otherHost, new_r_map);

			new_u_map.put(self, SimClock.getTime());
			u_table.put(otherHost, new_u_map);
		}
	}

	public double updateQValue(double q, double t, double old_value){
		return old_value + alpha*((q + t) - old_value);
	}

	private void updateConnectionTable(DTNHost otherHost){
		double time = (SimClock.getTime()) / secondsInTimeUnit;
		con_table.put(otherHost, time);
	}

	@Override
	public void update() {
		super.update();
		if (isTransferring() || !canStartTransfer()) {
			return; // transferring, don't try other connections yet
		}
		
		// Try first the messages that can be delivered to final recipient
		if (exchangeDeliverableMessages() != null) {
			return; // started a transfer, don't try others (yet)
		}
		tryOtherMessages();
	}

	public double get_q_value_forward(DTNHost destination, DTNHost other){
		if (q_table.containsKey(destination) && u_table.containsKey(destination) && b_table.containsKey(destination) && r_table.containsKey(destination)){
			double delta_time = SimClock.getTime() - u_table.get(destination).getOrDefault(other, 0.0);
			double q_value = q_table.get(destination).getOrDefault(other, 0.0);
			double r_value = r_table.get(destination).getOrDefault(other, 0.0);
			double b_value = b_table.get(destination).getOrDefault(other, 0.0);
			double q = Math.max(q_value + delta_time*r_value, b_value);
			if (q==0)
				return Double.MAX_VALUE;
			else
				return q;
		}
		else
			return Double.MAX_VALUE;
		
	}

	public Tuple<Message, Connection> tryOtherMessages() {
		List<Tuple<Message, Connection>> messages = 
			new ArrayList<Tuple<Message, Connection>>(); 
	
		Collection<Message> msgCollection = getMessageCollection();

		for (Message m : msgCollection) {
			DTNHost destination = m.getTo();
			if (q_table.containsKey(destination)){

				double min = Double.MAX_VALUE;
				Connection min_connection = null;

				for (Connection con : getConnections()) {
					DTNHost other = con.getOtherNode(getHost());
					double q_value = get_q_value_forward(destination, other);
					if(q_value <= min){
						min = q_value;
						min_connection = con;
					}
				}
				double self_q_value = get_q_value_forward(destination, getHost());
				if (self_q_value < min){
					min = self_q_value;
					min_connection = null;
				}

				if (min_connection != null){
					messages.add(new Tuple<Message, Connection>(m,min_connection));
				}
			}
			else{
				List<Connection> cons = getConnections();
				if (!cons.isEmpty()){
					Connection first = cons.get(0);
					messages.add(new Tuple<Message, Connection>(m, first));
				}
			}
		}

		if (messages.size() == 0) {
			return null;
		}

		this.sortByQueueMode(messages);
		return tryMessagesForConnected(messages);
	}

	public void updateQTableForwarding(DTNHost destination, DTNHost other, BasePQRouter othRouter, Message m){
		// System.out.println("From: " + getHost() + "   To: " + other + "    Desitnation: " + destination);

		Message init_message = getMessage(m.getId());

		// System.out.println("Message Recieved At: " + init_message.getReceiveTime());

		// double q = last_con_table.getOrDefault(other, 0.0);
		double q = (SimClock.getTime() - init_message.getReceiveTime()) / secondsInTimeUnit;
		double t = othRouter.get_min_for_destination(destination);
		if (q_table.containsKey(destination)){
			double old_value = q_table.get(destination).getOrDefault(other, 0.0);
			double new_value = updateQValue(q, t, old_value);
			q_table.get(destination).put(other, new_value);

			double b_value = b_table.get(destination).getOrDefault(other, 0.0);
			double delta_q = (q+t) - old_value;
			b_table.get(destination).put(other, Math.min(b_value, new_value));
			if (delta_q < 0){
				double last_edited_time = u_table.get(destination).get(other);
				double delta_time = (SimClock.getTime() - last_edited_time) / secondsInTimeUnit;
				double delta_r_value = delta_q / (delta_time);
				double new_r_value = r_table.get(destination).get(other) + (beta * delta_r_value);
				r_table.get(destination).put(other, new_r_value);
			}
			else if(delta_q > 0){
				double new_r_value = gamma * r_table.get(destination).getOrDefault(other, 0.0);
				r_table.get(destination).put(other, new_r_value);
			}
			u_table.get(destination).put(other, SimClock.getTime());

		}
		else{
			Map<DTNHost, Double> inner = new HashMap<DTNHost, Double>();
			double new_value = q + t;
			inner.put(other, new_value);
			q_table.put(destination, inner);

			Map<DTNHost, Double> new_b_map = new HashMap<DTNHost, Double>();
			Map<DTNHost, Double> new_r_map = new HashMap<DTNHost, Double>();
			Map<DTNHost, Double> new_u_map = new HashMap<DTNHost, Double>();

			new_b_map.put(other, new_value);
			b_table.put(destination, new_b_map);

			double curr_time = SimClock.getTime() / secondsInTimeUnit;

			double delta_r_value = new_value / curr_time;
			new_r_map.put(other, beta * delta_r_value);
			r_table.put(destination, new_r_map);

			new_u_map.put(other, SimClock.getTime());
			u_table.put(destination, new_u_map);

//			System.out.println(getHost() + " --> " + other + " DEstinatio:" + destination + " q=" + q + " old=" + "NULL" + " new=" + new_value);
		}
	}

	public double get_min_for_destination(DTNHost destination){
		double min = Double.MAX_VALUE;
		if (q_table.containsKey(destination)){
			Map<DTNHost, Double> inner = q_table.get(destination);
			for (Map.Entry<DTNHost, Double> f : inner.entrySet()) {
				Double innerValue = f.getValue();
				if (innerValue < min)
					min = innerValue;
			}
		}
		if (min == Double.MAX_VALUE)
			min = 0;
		return min;
	}

	@Override
	public RoutingInfo getRoutingInfo() {
		RoutingInfo top = super.getRoutingInfo();
		RoutingInfo ri = new RoutingInfo(String.format("Update Counter = %d", counter));
		top.addMoreInfo(ri);
		return top;
	}

	// @Override
	// protected void transferDone(Connection con) {
	// 	DTNHost other = con.getOtherNode(getHost());
	// 	BaseQRouter othRouter = (BaseQRouter)other.getRouter();
	// 	updateQTableForwarding(destination, other, othRouter);
	// 	System.out.println("Done");
	// }


	@Override
	public Message messageTransferred(String id, DTNHost from) {
		Message m = super.messageTransferred(id, from);
		BasePQRouter fromRouter = (BasePQRouter)from.getRouter();
		DTNHost destination = m.getTo();
		fromRouter.updateQTableForwarding(destination, getHost(), this, m);
		// System.out.println("Done");
		return m;
	}

	public Map<DTNHost, Map<DTNHost, Double>> getQTable(){
		return q_table;
	}

	public Map<DTNHost, Double> getConnectionTable(){
		return con_table;
	}
	
	public Map<DTNHost, Double> getLastConnectionTable(){
		return last_con_table;
	}

	public void putNewQValueForKnownDestination(DTNHost destination, DTNHost other, double new_value){
		q_table.get(destination).put(other, new_value);
	}

	public void putNewQValueForUnknownDestination(DTNHost destination, DTNHost other, double new_value){
		Map<DTNHost, Double> inner = new HashMap<DTNHost, Double>();
		inner.put(other, new_value);
		q_table.put(destination, inner);
	}

}