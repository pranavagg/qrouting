package routing;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import routing.util.RoutingInfo;
import util.Tuple;
import core.Connection;
import core.DTNHost;
import core.Message;
import core.Settings;
import core.SimClock;

import org.apache.commons.math3.distribution.PoissonDistribution;

public class CQRouter extends BaseCQRouter {

	public CQRouter(Settings s) {
		super(s);
	}

	protected CQRouter(CQRouter r) {
		super(r);
	}

	@Override
	public MessageRouter replicate() {
		CQRouter r = new CQRouter(this);
		return r;
	}


	@Override
	public Tuple<Message, Connection> tryOtherMessages() {
		List<Tuple<Message, Connection>> messages = 
			new ArrayList<Tuple<Message, Connection>>(); 
	
		Collection<Message> msgCollection = getMessageCollection();
		
		/* for all connected hosts collect all messages that have a higher
		   probability of delivery by the other host */
		
		for (Message m : msgCollection) {
			Tuple<Message, Connection> max_tuple=null;
			double max = 0;
			DTNHost destination = m.getTo();
			for (Connection con : getConnections()) {
				DTNHost other = con.getOtherNode(getHost());
				CQRouter othRouter = (CQRouter)other.getRouter();
				
				double self, otherProb;
				
				if (othRouter.isTransferring()) {
					continue; // skip hosts that are transferring
				}
				if (othRouter.hasMessage(m.getId())) {
					continue; // skip messages that the other one has
				}
				self = get_probability(destination, other);
				otherProb = othRouter.get_probability(destination, getHost());
				if (self < otherProb){
					if (otherProb > max){
						max = otherProb;
						max_tuple = new Tuple<Message, Connection>(m,con);
					}
				}
			}
			if (max_tuple != null){
				messages.add(max_tuple);
			}
		}
		
		if (messages.size() == 0) {
			return null;
		}
		
//		Collections.sort(messages, new TupleComparator());
		
		this.sortByQueueMode(messages);
		return tryMessagesForConnected(messages);	// try to send messages
	}
	
	private double getPoissonProbability(double mean, int x){
		PoissonDistribution p = new PoissonDistribution(mean);
		return p.probability(x);
	}
	
	private double get_probability_exclusive(DTNHost dest, DTNHost y){
		double max=0;
		Map<DTNHost, Map<DTNHost, Double>> q_table = getQTable();
		Map<DTNHost, Double> con_table = getConnectionTable();
		for (Map.Entry<DTNHost, Double> f : q_table.get(dest).entrySet()) {
			DTNHost a = f.getKey();
			Double innerValue = f.getValue();  // Qx(d, a)
			
			if (a.equals(y))
				continue;
			else if (a.equals(getHost()))
				continue;
			else{
				double self_prob, transitive, total, q;
				int time;
				
				if (q_table.containsKey(a)){
					if (q_table.get(a).containsKey(getHost()))
						q = q_table.get(a).get(getHost());  // Qx(a, x)
					else
						continue;
				}
				else
					continue;
				if (con_table.containsKey(a))
					time = (int)((SimClock.getTime() / secondsInTimeUnit) - con_table.get(a));  // lx(a)
				else
					continue;
				self_prob = getPoissonProbability(q, time);
				
				double q_dash = innerValue - q;
				if (q_dash > 0){
					transitive = getPoissonProbability(q_dash, (int) q_dash);
				}
				else
					continue;
				total = self_prob * transitive;
				if (total > max)
					max = total;
			}	
		}
		return max;
	}
	
	private double get_direct_probability(DTNHost dest){
		Map<DTNHost, Map<DTNHost, Double>> q_table = getQTable();
		Map<DTNHost, Double> con_table = getConnectionTable();
		double q, p;
		int time;
		if (q_table.get(dest).containsKey(getHost())){
			q = q_table.get(dest).get(getHost());  // Qx(d, x)
			if (con_table.containsKey(dest)){
				time = (int)((SimClock.getTime() / secondsInTimeUnit) - con_table.get(dest));  // lx(d)
				p = getPoissonProbability(q, time);
				return p;
			}
		}
		return 0;
	}
	
	public double get_probability(DTNHost dest, DTNHost y){
		Map<DTNHost, Map<DTNHost, Double>> q_table = getQTable();
		if (q_table.containsKey(dest)){
			double exclusive = get_probability_exclusive(dest, y);
			double direct = get_direct_probability(dest);
			if (exclusive > direct)
				return exclusive;
			else
				return direct;
		}
		else{
			return 0;
		}
	}
}
