package routing;

import core.DTNHost;
import core.Message;
import core.Settings;
import core.SimClock;
import java.util.Map;


public class BaseDualQRouter extends BaseQRouter {
	public BaseDualQRouter(Settings s) {
		super(s);
	}

	protected BaseDualQRouter(BaseDualQRouter r) {
		super(r);
	}

	@Override
	public MessageRouter replicate() {
		BaseDualQRouter r = new BaseDualQRouter(this);
		return r;
	}

	@Override
	public Message messageTransferred(String id, DTNHost from) {
		Message m = super.messageTransferred(id, from);
		DTNHost source = m.getFrom();
		BaseDualQRouter fromRouter = (BaseDualQRouter)from.getRouter();

		// System.out.println("Source: " + source + "    from: " + from + "    message: " + m);

		backward_exploration(source, from, fromRouter, m);

		return m;
	}

	public void backward_exploration(DTNHost source, DTNHost from, BaseDualQRouter fromRouter, Message m){
		double q = fromRouter.get_other_router_q_value(m);
		double t = fromRouter.get_min_for_destination(source);
		Map<DTNHost, Map<DTNHost, Double>> q_table = getQTable();

		if (q_table.containsKey(source)){
			double old_value = q_table.get(source).getOrDefault(from, 0.0);
			double new_value = updateQValue(q, t, old_value);
			putNewQValueForKnownDestination(source, from, new_value);
		}
		else{
			double new_value = q + t;
			putNewQValueForUnknownDestination(source, from, new_value);
		}

	}

	public double get_other_router_q_value(Message m){
		Message init_message = getMessage(m.getId());
		double q = (SimClock.getTime() - init_message.getReceiveTime()) / secondsInTimeUnit;
		return q;
	}
}