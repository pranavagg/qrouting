package routing;
import java.lang.Math.*;
import core.*;
import util.*;
import java.util.*;
import static java.lang.Math.pow;
import java.io.*;
import java.io.File;
import java.io.IOException;
import javax.swing.*;
import java.text.DecimalFormat;
import java.awt.event.*;

public class newRouter extends ActiveRouter implements MovementListener
{	

public Map<DTNHost,Integer> home;
private Map<Double,Integer> history;
private Map<Double,Double> speedTable;
private Map<DTNHost,Integer> map3;
private double max;
private double min;
private double latest10speeds[];
private  int x;
private  int y;
private DTNHost node;
private double acc;
private double prevSpeed;
private double Stability;int first,second;
private int count;
private double prevTime;

public newRouter(Settings s)
{
	super(s);
	initmaps();
}
protected newRouter(newRouter r) 
{
		super(r);
		initmaps();
}
//intialize constants
private void initmaps()
{
	
	//data structure for storing history
	this.history = new LinkedHashMap<Double,Integer>(100)
	{
		protected boolean removeEldestEntry(Map.Entry eldest) 
		{
		    return size() > 100;
		}
	};
	this.home = new HashMap<DTNHost,Integer>(200) ;
	this.speedTable = new LinkedHashMap<Double,Double>(10)
	{
		protected boolean removeEldestEntry(Map.Entry eldest) 
		{
		    return size() > 10;
		}
	};
	this.count = 0;
	this.prevTime=0;
}

@Override
	public void update() //update every router
	{
		super.update();
		if (isTransferring() || !canStartTransfer()) 
		{
			return; // can't start a new transfer
		}
		else if (exchangeDeliverableMessages() != null)
		{
			return;
		}
		tryOtherMessages();
	}

	private Tuple<Message, Connection> tryOtherMessages()
	{	
		List<Tuple<Message, Connection>> messages = new ArrayList<Tuple<Message, Connection>>();
		Collection<Message> msgCollection = getMessageCollection();
		for (Connection con : getConnections()) 
		{
			
			DTNHost me = getHost();
			DTNHost other = con.getOtherNode(getHost());
			newRouter othRouter = (newRouter)other.getRouter();
			if(othRouter.isTransferring())
			{continue;}
			for(Message m: msgCollection)
			{
				if (othRouter.hasMessage(m.getId())) 
				{continue;}
				DTNHost dest = m.getTo();
				
				if( calculateMetric(dest,other)>0.6 )
				{	
					//System.out.println(this.home);
					//System.out.println("history: "+this.history);
					//if(calculateMetric(dest,other) >calculateMetric(dest,me)) <--- for other cases when it is not to be kept constant
					messages.add(new Tuple<Message,Connection>(m,con));
				}
			
			}
			if(messages.size() == 0)
			return null;			
		}
		Collections.sort(messages, new TupleComparator());
		
		return tryMessagesForConnected(messages);
}
		
private class TupleComparator implements Comparator <Tuple<Message, Connection>>
 {
	public int compare(Tuple<Message, Connection> tuple1,Tuple<Message, Connection> tuple2) 
	{
		// delivery probability of tuple1's message with tuple1's connection
		double p1 = ((newRouter)(getHost().getRouter())).calculateMetric(tuple1.getKey().getTo(),tuple1.getValue().getOtherNode(getHost()));
		
		//  tuple2...
		double p2 = ((newRouter)(getHost().getRouter())).calculateMetric(tuple2.getKey().getTo(),tuple2.getValue().getOtherNode(getHost()));
		// bigger probability should come first
		if (p2-p1 == 0) 
		{
		//equal probabilities -> let queue mode decide 
		return compareByQueueMode(tuple1.getKey(), tuple2.getKey());
		}
		else if (p2-p1 < 0) 
		{
			return -1;
		}
		else
		{
			return 1;
		}
	}
}
//intialize homemap
public void setHomeMap(Map home1)
{
	home.putAll(home1);
}
@Override
public void changedConnection(Connection con)
{
	map3 = new HashMap<DTNHost,Integer>();
	if (con.isUp()) //merge maps 
	{
		DTNHost otherHost = con.getOtherNode(getHost());
		newRouter othHost = (newRouter)otherHost.getRouter();
		map3 = othHost.home;
		((newRouter)(getHost().getRouter())).home.putAll(map3);
	}
	
}
//calculating metrics
public double calculateMetric(DTNHost dest,DTNHost nextHost)
{
	double angleMetric=0; 
	newRouter othRouter = (newRouter)nextHost.getRouter();
	double m=slopeSD(dest);
	double distMetric = distanceFromSD(dest,nextHost);
	distMetric = 1.0/distMetric;
	int predictedLoc = predictNextLocation(othRouter);
	angleMetric = calcAngle(predictedLoc,nextHost,m,dest);
	double p=0.2*distMetric + 0.4*angleMetric + 0.4*othRouter.Stability;	
	
	return(p);

}

private double distanceFromSD(DTNHost dest,DTNHost nextHost)
{
	Coord destLoc = findMid(getHomeGrid(dest));
	Coord s = this.getHost().getLocation();
	double x1 = s.getX();
	double y1 = s.getY();
	double x2 = destLoc.getX();
	double y2 = destLoc.getY();
	//find line constants
	//System.out.println(x1+" "+y1);
	double a = y1 - y2;
	double b = x2 - x1;
	double c = y2*x1 - y1*x2;
	Coord nextHostLoc = nextHost.getLocation();
	double x3 = nextHostLoc.getX();
	double y3 = nextHostLoc.getY();
	double dist = a*x3 + b*y3 + c;
	if(dist<0) dist = -dist;
	dist = dist/(Math.pow((a*a+b*b),0.5));
	return dist;
}
private int getHomeGrid(DTNHost dest)
{
	if(this.home.containsKey(dest))
	return this.home.get(dest);
	else return 742;
}
private double slopeSD(DTNHost dest)
{
	Coord destLoc = findMid(getHomeGrid(dest));
	Coord s = this.getHost().getLocation();
	double x1 = s.getX();
	double y1 = s.getY();
	double x2 = destLoc.getX();
	double y2 = destLoc.getY();
	return ((y2-y1)/(x2-x1));

}
private Coord findMid(int gridNo)//finding mid point in a cell in world
{
	double y = (int)(gridNo/45)*100+50;
	double x = ((gridNo%45)-1)*100+50;
	Coord c = new Coord(x,y);
	return c;
}

//calculating angle metric
private double calcAngle(int nextgrid,DTNHost nextHost,double m,DTNHost dest)
{

	double x1,y1,x2,y2,X1,Y1,X2,Y2,angle1,xd,yd;
	//Current location of Neighboring node
	Coord currLoc = nextHost.getLocation();
	x2 = currLoc.getX();
	y2 = currLoc.getY();	
	
	//Predicted location of Neighboring node
	Coord nextLoc = findMid(nextgrid);
	x1 = nextLoc.getX();
	y1 = nextLoc.getY();

	//Location of Destination node
	Coord destCord = findMid(getHomeGrid(dest));
	//Coord destCord=dest.getLocation();
	xd=destCord.getX();
	yd=destCord.getY();
	
	//MODULE 1
	//Angle made by SD line and rotating and shifting axis
	angle1=Math.atan(m);
	
	X1= (x1-xd)*Math.cos(angle1)-(y1-yd)*Math.sin(angle1);
	Y1= (x1-xd)*Math.sin(angle1)+(y1-yd)*Math.cos(angle1);
	
	X2= (x2-xd)*Math.cos(angle1)-(y2-yd)*Math.sin(angle1);
	Y2= (x2-xd)*Math.sin(angle1)+(y2-yd)*Math.cos(angle1);
	
	//MODULE 2: CHECKING IN WHICH REGION IT LIES W.R.T DESTINATION
	int region=0;
	//1st quadrant
	if((X2>=0 && Y2>=0)&&((X1-X2)<0 && (Y1-Y2)<0))
		region=1;

	//2nd quadrant
	if((X2<0 && Y2>0)&&((X1-X2)>0 && (Y1-Y2)<0))
		region=2;

	//3rd quadrant
	if((X2<0 && Y2<0)&&((X1-X2)>0 && (Y1-Y2)>0))
		region=3;

	//4th quadrant
	if((X2>0 && Y2<0)&&((X1-X2)<0 && (Y1-Y2)>0))
		region=4;

	//MODULE 3: SCRUTINIZING NODES W.R.T. EQUATION X=+/-Y 
	
	double M;
	double ang;
	//Rejected Nodes
	if(region==0)
		return 1;
		
	M=(Y2-Y1)/(X2-X1);
	if(region==2|| region==4)
		M=M*(-1); // to get the acute angle
	ang=Math.atan(M);
	//Cases when angle with y axis should be considered when node lies in the region of x=+-y line which is away from Destination node
	if(((region==1) && (X2<Y2))||((region==2) && (-X2<Y2)) || ((region==3) && (X2>Y2)) ||((region==4) && (X2<-Y2)) )
	{
		ang=1.57-ang;
	}
	
	double result =Math.cos(ang);
	result=10*result;
	return result;
	}	
public int predictNextLocation(newRouter othRouter)
{
	Map map1 = new HashMap<Integer,Integer>();
	Collection c = othRouter.history.values();
	
	int gridNO,val;
	Set<Double> set = othRouter.history.keySet();
	Iterator i = set.iterator();
	
	while(i.hasNext())
	{
		//matching two recent locations
		//if(othRouter.history.get(i.next())==second)
		//{
		//if(i.hasNext())
			//{
				if(othRouter.history.get(i.next())==first)
				{
					if(i.hasNext())
					{
						gridNO = othRouter.history.get(i.next());
						if(map1.containsKey(gridNO))
						{
							val=(Integer)map1.get(gridNO);
							map1.remove(gridNO);
							map1.put(gridNO,val+1);
						}
						else
						{
							map1.put(gridNO,1);
						}
					}
					else break;
				}
			//}
		//}
	}

	//find location which occured maximum number of times
	Map.Entry<Integer,Integer> maxentry = null;
	Map.Entry<Integer,Integer> entry2 = null;

	Iterator entries = map1.entrySet().iterator();
	if(entries.hasNext()) 
	maxentry = (Map.Entry) entries.next();
	while (entries.hasNext()) 
	{
		entry2 = (Map.Entry) entries.next();
		if((Integer)entry2.getValue()>(Integer)maxentry.getValue())
		maxentry = entry2;
	}
	if(maxentry==null)
	gridNO=0;
	else
	gridNO = (Integer)maxentry.getKey();
	return gridNO;
	
}
	private void checkStability(DTNHost host, Coord destination, double speed1)
	{
			newRouter myRouter = (newRouter)host.getRouter();
			int c1=0;
			int c2=0;
			double a,b;
			Coord loc=host.getLocation();
			int grid = assignGrid(loc);
			Double time = SimClock.getTime();
			myRouter.history.put(time,grid);	
			
			//executed only on first two speeds
			if(myRouter.count<=2)
			{
				if(speed1<myRouter.prevSpeed)
				myRouter.Stability = 1;
				else if((speed1>myRouter.prevSpeed)&&(speed1<1.5*myRouter.prevSpeed))
				myRouter.Stability = 0.75;
				else if((speed1>1.5*myRouter.prevSpeed)&&(speed1<2.0*myRouter.prevSpeed))
				myRouter.Stability = 0.5;
				else
				myRouter.Stability = 0.25;
			}
		
			else
			{	
					myRouter.speedTable.remove(9.9); //scrap entry, filled during initialization
					
					Collection c=myRouter.speedTable.values();
					Object m1 = Collections.max(c);
					Object m2 = Collections.min(c);
					
					String str1 = m1.toString(); 
					myRouter.max = Double.valueOf(str1).doubleValue();
					
					String str2 = m2.toString(); 
					myRouter.min = Double.valueOf(str2).doubleValue();
					
					Set<Double> set = myRouter.speedTable.keySet();
					Iterator i= set.iterator();
					
					double avg= ( myRouter.max + myRouter.min)/2;
					if((avg-speed1)>0.9 )
							myRouter.Stability = myRouter.Stability+(myRouter.Stability)*0.2;
					else if((speed1-avg)>0.9  && myRouter.Stability>0 )
							myRouter.Stability = myRouter.Stability -(myRouter.Stability)*0.2;
										
					while(i.hasNext())
					{
						double sp=myRouter.speedTable.get(i.next());
						if(sp<speed1)
						c1++;
						if(sp>speed1)
						c2++;
					}
					if(c1>c2) 
					{
						a= pow(0.91,c1);
						myRouter.Stability=myRouter.Stability*a;
					}
					if(c1<c2)
					{
						b= pow(1.09,c2);
						myRouter.Stability=myRouter.Stability*b;
					}
				}
			myRouter.count=myRouter.count+1;
			myRouter.speedTable.put(time,speed1);
	}
	
@Override
	public void newDestination(DTNHost host, Coord destination, double speed1)
	{
		newRouter myRouter = (newRouter)host.getRouter();
		Double time = SimClock.getTime();
		if(myRouter.prevTime !=time)
		{
			Coord loc=host.getLocation();
			int grid = assignGrid(loc);
			checkStability(host, destination, speed1);			
			myRouter.second=myRouter.first;
			myRouter.first=grid;				
			myRouter.prevTime=time;
		}
					
	}
	

	@Override 
	public void initialLocation(DTNHost host,Coord destination)
	{
		Double time = SimClock.getTime();
		newRouter myRouter = (newRouter)host.getRouter();
		Coord loc=host.getLocation();
		int grid = assignGrid(loc);
		
		myRouter.history.put(time,grid);
		myRouter.prevSpeed = 0;	
		myRouter.home.put(host,grid);
		myRouter.first=grid;
		myRouter.speedTable.put(9.9,0.0);
		
	}

	public int assignGrid(Coord loc)
	{	
		double xloc = loc.getX();
		double yloc = loc.getY();
		int a = (int)(xloc/100.0);
		int b = (int)(yloc/100.0);
		return ( a+1+b*45);
	}
		
	@Override
	public MessageRouter replicate() 
	{
		newRouter r = new newRouter(this);
		return r;
	}
	
}
